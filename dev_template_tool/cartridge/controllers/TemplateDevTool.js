'use strict';

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
//var response = require('app_lacoste_core/cartridge/scripts/util/Response');

var ProductMgr = require('dw/catalog/ProductMgr');
var OrderMgr = require('dw/order/OrderMgr');
var CustomerMgr = require('dw/customer/CustomerMgr');
var StoreMgr = require('dw/catalog/StoreMgr');
var Pipeline = require('dw/system/Pipeline');
var v = require('GetValueOrUndefined'); 

function show() {
	app.getView().render("dev_template_tool/template_tool");
};

var httpParameterFunctions = [
	{
		name	: 'pid',
		func 	: function (context, httpParameter) {
			context.Product = ProductMgr.getProduct(httpParameter.stringValue);			  
			return true;
		}
	},
	{
		name	: 'oid',
		func 	: function (context, httpParameter) {
			context.Order = OrderMgr.getOrder(httpParameter.stringValue);			  
			return true;
		}
	},
	{
		name 	: 'customer',
		func 	: function (context, httpParameter) {
			context.Customer = customerMgr.getCustomerByNumber(httpParameter.stringValue);			  
			return true;
		}
	},
	{
		name 	: 'login',
		func 	: function (context, httpParameter) {
			context.Product = CustomerMgr.getCustomerByLogin(httpParameter.stringValue);			  
			return true;
		}
	},
	{
		name	: 'store',
		func 	: function (context, httpParameter) {
			context.Store = StoreMgr.getStore(httpParameter.stringValue);			  
			return true;
		}
	},
	{
		name	: 'locale',
		func 	: function (context, httpParameter) {
			request.setLocale(httpParameter.stringValue);
			return true;
		}
	},
	{
		name	: 'pipeline',
		func 	: function (context, httpParameter) {
			Pipeline.execute(httpParameter.stringValue, context);
			return true;
		}
	},
	{ // Custom Fields
		regexp : {
			key : /^pdict-(\w+?)$/,
			value : /.+/
		},
		func : function (context, matchs, httpParameter) {
			if (!empty(matchs)) {
				if ('key' in matchs) {
					var key = matchs.key[1];
				} else {
					var key = matchs[1];
				}
				if (matchs.key.length == 2) {
					var pdictFieldCodeKey = "pdict-" + key; 
					try {
						context[key] = eval(httpParameter.stringValue);
					} catch (e) {
						context[key] = e;  
					}
					return true;
				}				
			}
			return false;
	  }
  }
];


function tool () {
	
	try {
	
	var systemParam = ['template', 'mailTo', 'local'];
	var httpParameterMap = request.getHttpParameterMap();
	var httpParameterKeys = httpParameterMap.getParameterNames();
	var context = {};
	
	// Creating the pdict
	for (var iter = httpParameterKeys.iterator(); iter.hasNext();) {
		var parameterKey = iter.next();
		
		for (var i in httpParameterFunctions) {
			var httpParameterFunction = httpParameterFunctions[i];
			var httpParameter = httpParameterMap.get(parameterKey);

			if (!httpParameter.empty && !empty(parameterKey) && systemParam.indexOf(parameterKey) == -1) {
				if ('name' in httpParameterFunction && parameterKey == httpParameterFunction.name) {
					// Basic fields
					if (typeof(httpParameterFunction.func) == 'function') {
						if (httpParameterFunction.func(context, httpParameter)) {
							break;//if the function returns true we continue with the next parameter				
						}
					}
				} else if ('regexp' in httpParameterFunction) {
					// Custom fields
					if (typeof (httpParameterFunction.regexp) == 'object') {
						var matchs = {};
						if (httpParameterFunction.regexp.key && parameterKey) {
							matchs.key = parameterKey.match(httpParameterFunction.regexp.key);
						}
						if (httpParameterFunction.regexp.value && httpParameterFunction.stringValue) {
							matchs.value = httpParameterFunction.stringValue.match(httpParameterFunction.regexp.key);
						}
					} else {
						matchs = httpParameterFunction.regexp.key.match(parameterKey);
					}
					if (typeof(httpParameterFunction.func) == 'function') {
						if (httpParameterFunction.func(context, matchs, httpParameter)) {
							break;//if the function returns true we continue with the next parameter					
						}
					}
				}				
			}
		}
	}
	
	// Mailing
	var mailTo = !request.httpParameterMap.mailTo.empty ? request.httpParameterMap.mailTo.stringValue : null;
	try {
		if (mailTo) {
			var t = new dw.util.Template(template);
		  	var content = t.render(context);
		 	sendMail(mailTo.split(";"), "devtool@dev02-lacoste.com", "test", content);
		}
	} catch (e) {
		var ex = e;
	}

	// Rendering
	var template = !request.httpParameterMap.template.empty ? request.httpParameterMap.template.stringValue : null;
		if (template) {
			app.getView(context).render(template);
		} else {
			response.getWriter().print(JSON.stringify(pdict));
		}
	} catch (e) {
		var ex =e;
		for (var key in e) {
			if (typeof(e[key]) != 'function') {
				response.getWriter().print("<p>" + key + " : " + e[key] + "</p>");				
			}
		}
	}

};


function sendMail(mailsTo, mailFrom, subject, content) {
	  var mail = new dw.net.Mail();
	  for (var index in mailsTo) {
		  mail.addTo(mailsTo[index]);
	  }
	  mail.setFrom(mailFrom || "no-reply@demandeware.com");
	  if (subject) {
		  mail.setSubject(subject); 
	  }
	  mail.setContent(content);

	  return mail.send();//returns either Status.ERROR or Status.OK, mail might not be sent yet, when this method returns
};

exports.Show = guard.ensure(['get'], show);
exports.Tool = guard.ensure(['post'], tool);

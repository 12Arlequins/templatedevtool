var storage = localStorage;
var CONFIGURATION_KEY = "CONFIG-";
var CONFIGURATION_KEYS = "CONFIG";

var $loadConfigurationModel = $('#loadConfigurations .model');

function getConfigurationKeys() {
	var keys = storage.getItem(CONFIGURATION_KEYS);
	var keysObj = [];
	if (keys) {
		try {
			keysObj = JSON.parse(keys);
		} catch (e) {
			console.log(e);
			alert('An error occured, the exception in the console.\nGood luck.');
		}
	}
	return keysObj;
};

function CreateLoadingBlock(name) {
	var $clone = $loadConfigurationModel.clone();
	
	$clone.find(".name").text(name);
	$clone.find("input").attr('name', name);
	$clone.removeClass('model');
}

function AddConfigurationKey(name) {
	var keysObj = getConfigurationKeys();
	
	keysObj.push(name);
	storage.setItem(CONFIGURATION_KEYS, JSON.stringify(keysObj));
};

function RemoveConfigurationKey(name) {
	var keysObj = getConfigurationKeys();	
	var index = keysObj.indexOf(name);
	if (index != -1) {
		 keysObj.splice(index, 1);
	}
	storage.setItem(CONFIGURATION_KEYS, JSON.stringify(keysObj));
};

function SaveConfiguration(name) {
	var obj = {
	};

	var inputs = $('#templateAttributeForm select, templateAttributeForm input');
	inputs.each(function (elem) {
		var type = elem.attr('type');
		var name = elem.attr('name');
		if (type != 'submit' && type != 'reset' && name && name.length > 0)
			obj[name] = elem.val();
	});

	storage.setItem(CONFIGURATION_KEY + name,  JSON.stringify(obj));
	AddConfigurationKey(name);
	CreateLoadingBlock(name);
};

function LoadConfiguration(name) {
	var objAsStr = storage.getItem(CONFIGURATION_KEY + name);

	try {
		var obj = JSON.parse(objAsStr);
		var reg = /^pdict-/;
		
		for (var key in obj) {
			if (key.search(reg) == 0) {
				createNewCustomField(name, obj[key])
			} else {
				var $elem = $("#templateAttributeForm *[name=" + key + "]");
				if ($elem) {
					$elem.val(obj[key]);					
				}
			}
		}
		return;
		
	} catch (e) {
		console.log(e);
	}
	alert('Configuration "' + name + '" could not be found');
};

$('#saveConfigurations').on('submit', function (e) {
	e.preventDefault();
	var nameInput = $(this).find('.save-name');
	
	if (nameInput) {
		var nameValue = nameInput.val();
		
		if (nameValue && nameValue.length > 0) {
			SaveConfiguration(nameValue);
			return false
		}
	}
	alert('Invalid configuration name');
	return false;
});
var $document = $(document);
var $showHideButton = $('#show-hide');
var $reload = $('#reload');
var $clear = $('#clear');
var $fieldset = $('#fieldset');
var $form = $('#templateAttributeForm');
var $tabs = $("#tabs a");
var $tabContent = $(".tab-content");


$showHideButton.on('click', function (e) {
	e.preventDefault();
	$fieldset.toggle();
});

$reload.on('click', function (e) {
	e.preventDefault();
	$form.submit();
});

$clear.on('click', function (e) {
	e.preventDefault();
	$form[0].reset();
});

$tabs.on('click', function () {
	$tabs.removeClass('selected');
	$tabContent.removeClass('selected');
	var $this = $(this);
	$this.addClass('selected');
	$('#' + $this.data('target')).addClass('selected');
});


/************************************************/
/************  CUSTOM                ************/
/************************************************/

var $customModel = $('#custom-model');

function initNewField($field, name, value) {
	var $this = $field;
	
	if (!$this.hasClass('row')) {
		throw new Error('initNewField parameter is suppose to be a row');
	}
	var pdictKeyInput = 		$field.find('.c1 input');
	var pdictValueTextarea = 	$field.find('.c2 textarea');
	var customAddButton = 		$field.find('.c3 .addCustomField');
	var customRemoveButton = 	$field.find('.c3 .removeCustomField');
	
	pdictKeyInput.val('');
	if (name) {
		pdictValueTextarea.attr("name", ("pdict-" + name) || '');		
	}
	pdictValueTextarea.val(value || '');
	
	pdictKeyInput.on('change', function (e) {
		var $this = $(this);
		var val = $this.val();
		if (val && val.length > 0) {
			pdictValueTextarea.removeClass('no-use');
			pdictValueTextarea.attr('name', 'pdict-' + val);			
		} else {
			pdictValueTextarea.addClass('no-use');
			pdictValueTextarea.attr('name', '');
		}
	});
	
	customRemoveButton.on('click', function (e) {
		e.preventDefault();
		$this.remove();
	});

	customAddButton.on('click', function (e) {
		e.preventDefault();
		createNewCustomField();
	});
};


function createNewCustomField(name, value) {	
	var $clone = $customModel.clone();
	$clone.attr('id', '');
	initNewField($clone, name, value);
	$clone.appendTo($customModel.parent());
	$customModel.appendTo($customModel.parent());
}

initNewField($customModel);